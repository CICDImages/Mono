ARG MONO_VERSION=5.8.0.127

FROM mono:$MONO_VERSION

# Install Git and NUnit Console, with version 2.6 and 3.x runners.

RUN apt-get update -q \
    && apt-get -q -y install git \
    && nuget install NUnit.Console \
    && rm -rf /var/lib/apt/lists/* /tmp/*

# Install SharpCover.

RUN git clone https://github.com/Didstopia/SharpCoverPlus.git \
    && cd SharpCoverPlus \
    && nuget restore \
    && msbuild /p:Configuration=Release \
    && mkdir -p /opt/SharpCoverPlus \
    && cp /SharpCoverPlus/SharpCoverPlus/bin/Release/* /opt \
    && rm -rf /SharpCoverPlus



WORKDIR /usr/src
